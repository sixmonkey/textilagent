import path from 'path'
import colors from 'vuetify/es5/util/colors'
import menus from './nuxt/config/menus'
import app from './nuxt/config/app'
import 'dotenv/config';

var fs = require('fs');
let version = 'unknown'
if (fs.existsSync('.version')) {
    version = fs.readFileSync('.version', 'utf8');
}

const baseURL = process.env.NODE_ENV === 'development' ? `${process.env.APP_URL}/api` : '/api'

export default {
    // Target: https://go.nuxtjs.dev/config-target
    target: 'static',

    // srcDir: https://nuxtjs.org/docs/configuration-glossary/configuration-srcdir/
    srcDir: 'nuxt',

    // watch: https://nuxtjs.org/docs/configuration-glossary/configuration-watch/
    watch: ['~/../app/**/*.php'],

    // Global page headers: https://go.nuxtjs.dev/config-head
    head: {
        titleTemplate: '%s - textilexchange',
        title: 'textilexchange',
        meta: [
            {charset: 'utf-8'},
            {name: 'viewport', content: 'width=device-width, initial-scale=1'},
            {hid: 'description', name: 'description', content: ''},
            {name: 'format-detection', content: 'telephone=no'}
        ],
        link: [
            {rel: 'icon', type: 'image/x-icon', href: '/favicon.ico'}
        ]
    },

    // Global CSS: https://go.nuxtjs.dev/config-css
    css: ['@/assets/scss/app.scss'],

    // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
    plugins: [
        '~/plugins/menuHelper.js',
        '~/plugins/colorHelper.js',
        '~/plugins/pluralHelper.js',
        '~/plugins/datatableToJsonApi.js',
        '~/plugins/vendor/vuetify-toast.js',
        '~/plugins/vendor/vee-validate.js',
        '~/plugins/vendor/lodash.js',
        {src: '~/plugins/axios', ssr: true},
        '~/plugins/auth.js',
        '~/plugins/buildBinds.js',
        '~/plugins/iconHelper.js',
        {src: '~/plugins/vendor/vue-chartjs.js', mode: 'client'},
        '~/plugins/vendor/vue-match-heights.js',
        '~/plugins/phone.js',
        {src: '~/plugins/vendor/vue-js-toggle-button.js', mode: 'client'}
    ],

    // Auto import components: https://go.nuxtjs.dev/config-components
    components: [
        {path: '~/components', prefix: 'app'},
        {path: '~/components/form', prefix: 'app-form'},
        {path: '~/components/btn', prefix: 'app-btn'},
    ],

    // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
    buildModules: [
        // https://go.nuxtjs.dev/eslint
        '@nuxtjs/eslint-module',
        // https://go.nuxtjs.dev/stylelint
        '@nuxtjs/stylelint-module',
        // https://go.nuxtjs.dev/vuetify
        '@nuxtjs/vuetify',
    ],

    // Modules: https://go.nuxtjs.dev/config-modules
    modules: [
        // https://go.nuxtjs.dev/axios
        '@nuxtjs/axios',
        // https://go.nuxtjs.dev/pwa
        '@nuxtjs/pwa',
        // https://go.nuxtjs.dev/content
        '@nuxt/content',
        // https://auth.nuxtjs.org/
        '@nuxtjs/auth-next',
        // https://github.com/dword-design/nuxt-route-meta
        'nuxt-route-meta',
        // https://github.com/patrickcate/nuxt-jsonapi
        'nuxt-jsonapi',
        // https://github.com/yariksav/vuetify-dialog
        'vuetify-dialog/nuxt',
        // https://phiny1.github.io/v-currency-field/
        [
            'v-currency-field/nuxt',
            {
                decimalLength: 2,
                autoDecimalMode: true,
                min: null,
                max: null,
                defaultValue: 0,
                valueAsInteger: false,
                allowNegative: true,
            },
        ],
    ],

    // Axios module configuration: https://go.nuxtjs.dev/config-axios
    axios: {
        // Workaround to avoid enforcing hard-coded localhost:3000: https://github.com/nuxt-community/axios-module/issues/308
        baseURL,
        credentials: true,
    },

    // https://github.com/patrickcate/nuxt-jsonapi
    jsonApi: {
        baseURL,
        axiosOptions: {
            withCredentials: true,
        }
    },

    // PWA module configuration: https://go.nuxtjs.dev/pwa
    pwa: {
        manifest: {
            lang: 'en'
        }
    },

    // Content module configuration: https://go.nuxtjs.dev/config-content
    content: {},

    // Vuetify module configuration: https://go.nuxtjs.dev/config-vuetify
    vuetify: {
        treeShake: false,
        theme: {
            themes: {
                options: {
                    customProperties: true,
                },
                light: {
                    primary: colors.blue.darken2,
                    accent: colors.grey.darken3,
                    secondary: colors.amber.darken3,
                    info: colors.teal.lighten1,
                    warning: colors.amber.base,
                    error: colors.deepOrange.accent4,
                    success: colors.green.accent4,
                    background: colors.grey.lighten4,
                },
                dark: {
                    primary: colors.blue.darken2,
                    accent: colors.grey.darken3,
                    secondary: colors.amber.darken3,
                    info: colors.teal.lighten1,
                    warning: colors.amber.base,
                    error: colors.deepOrange.accent4,
                    success: colors.green.accent3,
                    background: colors.grey.darken3,
                },
            },
        }
    },
    router: {
        middleware: ['auth'],
        parseQuery: (query) => {
            return require('qs').parse(query)
        },
        stringifyQuery: (query) => {
            return require('qs').stringify(query, {
                arrayFormat: 'comma',
                addQueryPrefix: true,
                encode: false,
                sort(a, b) {
                    return a.localeCompare(b);
                }
            })
        },
    },
    auth: {
        strategies: {
            laravelSanctum: {
                provider: 'laravel/sanctum',
                // url: 'http://165.227.135.24/api'
                url: process.env.APP_URL,
                endpoints: {
                    csrf: {
                        url: baseURL + '/csrf-cookie'
                    },
                    login: {
                        url: baseURL + '/login'
                    },
                    logout: {
                        url: baseURL + '/logout'
                    },
                    user: {
                        url: baseURL + '/user'
                    }
                },
            },
        },
        watchLoggedIn: true,
        watchUser: true,
    },
    publicRuntimeConfig: {
        menus,
        app: {...app, ...{version}},
        page: {
            default_size: process.env.PAGE_SIZE_DEFAULT ?? 25,
            max_size: process.env.PAGE_SIZE_MAX ?? 100,
        }
    },
    privateRuntimeConfig: {},

    // Build Configuration: https://go.nuxtjs.dev/config-build
    build: {
        transpile: ['vee-validate', 'vue-underscore', 'vuetify/lib'],
        loaders: {
            pugPlain: {
                basedir: path.resolve(__dirname, 'nuxt', 'assets', 'pug'),
            },
        }
    },

    // Build Configuration: https://go.nuxtjs.dev/config-build
    generate: {
        dir: 'public',
        exclude: [
            /^\/.+$/ // exclude anything but home (/) because we leave any routing to laravel
        ]
    }
}
