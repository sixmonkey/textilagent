<?php

use App\Http\Controllers\CompaniesController;
use App\Http\Controllers\CountriesController;
use App\Http\Controllers\CurrenciesController;
use App\Http\Controllers\DescriptionsController;
use App\Http\Controllers\InvoicesController;
use App\Http\Controllers\OrderItemsController;
use App\Http\Controllers\OrdersController;
use App\Http\Controllers\ShipmentsController;
use App\Http\Controllers\TypologiesController;
use App\Http\Controllers\UnitsController;
use App\Http\Controllers\UsersController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/version', function () {
    $file = base_path('.version');
    return File::exists($file) ? File::get($file) : 'unknown';
});

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user()->load('country');
});

Route::middleware('auth:sanctum')->group(function () {
    Route::apiResources([
        'users' => UsersController::class,
        'companies' => CompaniesController::class,
        'units' => UnitsController::class,
        'currencies' => CurrenciesController::class,
        'orders' => OrdersController::class,
        'shipments' => ShipmentsController::class,
        'invoices' => InvoicesController::class,
        'order-items' => OrderItemsController::class,
    ]);

    Route::resource('countries', CountriesController::class)->only([
        'index',
    ]);
    Route::resource('typologies', TypologiesController::class)->only([
        'index',
    ]);
    Route::resource('descriptions', DescriptionsController::class)->only([
        'index',
    ]);

    Route::get('orders/{order_id}/documents', [OrdersController::class, 'documents_index'])
        ->name('orders.documents.index');
    Route::get('orders/{order_id}/documents/{document_id}', [OrdersController::class, 'documents_show'])
        ->name('orders.documents.show');
    Route::post('orders/{order_id}/documents', [OrdersController::class, 'documents_store'])
        ->name('orders.documents.store');
    Route::delete('orders/{order_id}/documents/{document_id}', [OrdersController::class, 'documents_delete'])
        ->name('orders.documents.delete');

    Route::get('shipments/{shipment_id}/documents', [ShipmentsController::class, 'documents_index'])
        ->name('shipments.documents.index');
    Route::get('shipments/{shipment_id}/documents/{document_id}', [ShipmentsController::class, 'documents_show'])
        ->name('shipments.documents.show');
    Route::post('shipments/{shipment_id}/documents', [ShipmentsController::class, 'documents_store'])
        ->name('shipments.documents.store');
    Route::delete('shipments/{shipment_id}/documents/{document_id}', [ShipmentsController::class, 'documents_delete'])
        ->name('shipments.documents.delete');

    Route::get('invoices/{invoice_id}/pdf', [InvoicesController::class, 'pdf'])
        ->name('invoices.pdf');
});
