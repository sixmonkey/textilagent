<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
    <title>{{ config('app.name') }} Invoice {{ $invoice->invoice_number }}</title>
    <link rel="stylesheet" href="https://xiphe.net/din-5008-css/din-5008.css"/>
    <style>

        @page {
            size: A4;
            margin: 0;
            font-family: Helvetica, Arial, sans-serif;
        }

        th {
            text-align: left;
        }

        td, th {
            white-space: nowrap;
            padding: 0.5em 1.5em;
        }

        td:first-child, th:first-child {
            padding-left: 0;
        }

        td:last-child, th:last-child {
            padding-right: 0;
        }

        td.collapse, th.collapse {
            width: 1%;
        }

        tfoot {
            border-top: 1px solid #000;
            padding-top: 1em;
        }

        .right {
            text-align: right;
        }
    </style>
</head>

<body>
<din-5008>
    <briefkopf></briefkopf>
    <rucksendeangabe>
        {{ config('app.name') }} | {{ config('app.street') }} | {{ config('app.city') }}
    </rucksendeangabe>
    <anschriftzone>
        <strong>{{ $invoice->receiver->name }}</strong><br/>
        &nbsp;<br/>
        <small>
        {!! nl2br($invoice->receiver->address) !!}<br/>
        {{ $invoice->receiver->country->name }}
        </small>
    </anschriftzone>
    <informationsblock class="right">{{ $invoice->date->translatedFormat('l, j F Y') }}</informationsblock>
    <textfeld style="margin-right: 2cm">
        <h3>Invoice {{ $invoice->invoice_number }}</h3>
        <table style="width: 100%">
            <thead>
            <tr>
                <th class="collapse">Date</th>
                <th>Shipment #</th>
                <th class="collapse right">Amount</th>
            </tr>
            </thead>
            <tbody>
            @foreach($invoice->shipments as $shipment)
                <tr>
                    <td class="collapse">{{ $shipment->date->translatedFormat('j F Y') }}</td>
                    <td>{{ $shipment->title }}</td>
                    <td class="collapse right">
                        @money((int)$shipment->total_commission, $currency->code, true)
                    </td>
                </tr>
            @endforeach
            </tbody>
            <tfoot>
            <tr>
                <th colspan="2">Total</th>
                <td class="collapse right">
                    @money($invoice->total_commission, $currency->code, true)
                </td>
            </tr>
        </table>
    </textfeld>
</din-5008>
</body>
</html>
