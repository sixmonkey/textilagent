<?php

namespace Tests\Feature;

use App\Models\Invoice;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class InvoiceControllerTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    /**
     * testing the index.
     *
     * @return void
     */
    public function test_index(): void
    {
        $user = User::factory()
            ->create(['admin' => true])
            ->first();

        Sanctum::actingAs($user);

        Invoice::factory()->count(5)->create();

        $response = $this->getJson('/api/invoices?sort=receiver');
        $response
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonCount(5, 'data');
    }

    /**
     * testing the index.
     *
     * @return void
     */
    public function test_show(): void
    {
        $user = User::factory()
            ->create(['admin' => true]);

        Sanctum::actingAs($user);

        $invoice = Invoice::factory()->create();

        $response = $this->getJson('/api/invoices/' . $invoice->id);
        $response
            ->assertStatus(Response::HTTP_OK)
            ->assertJson(['data' => $invoice->toArray()]);
    }


    /**
     * testing creation of a invoice.
     *
     * @return void
     */
    public function test_store(): void
    {
        $response = $this->postJson('/api/invoices', []);
        $response
            ->assertStatus(Response::HTTP_UNAUTHORIZED);

        $otherUser = User::factory()
            ->create(['admin' => false]);

        Sanctum::actingAs($otherUser);

        $invoice = Invoice::factory()->make();

        $response = $this->postJson('/api/invoices', $invoice->toArray());
        $response
            ->assertStatus(Response::HTTP_FORBIDDEN);

        $admin = User::factory()
            ->create(['admin' => true]);

        Sanctum::actingAs($admin);

        $response = $this->postJson('/api/invoices', [
            'code' => '',
        ]);
        $response
            ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);

        $invoice = Invoice::factory()->make();

        $response = $this->postJson('/api/invoices', $invoice->toArray());
        $response
            ->assertStatus(Response::HTTP_CREATED);
        $this->assertDatabaseHas('invoices', $invoice->only(['date']));
    }


    /**
     * test deleting a invoice
     *
     * @return void
     */
    public function test_delete(): void
    {
        $invoice = Invoice::factory()->create();

        $response = $this->deleteJson('/api/invoices/' . $invoice->id);
        $response
            ->assertStatus(Response::HTTP_UNAUTHORIZED);

        $otherUser = User::factory()
            ->create(['admin' => false]);

        Sanctum::actingAs($otherUser);

        $response = $this->deleteJson('/api/invoices/' . $invoice->id);
        $response
            ->assertStatus(Response::HTTP_FORBIDDEN);

        $admin = User::factory()
            ->create(['admin' => true]);

        Sanctum::actingAs($admin);

        $response = $this->deleteJson('/api/invoices/' . $invoice->id);
        $response
            ->assertStatus(Response::HTTP_OK);

        $this->assertDatabaseMissing('invoices', ['id' => $invoice->id]);
    }
}
