<?php

namespace Tests\Feature;

use App\Models\Company;
use App\Models\Country;
use App\Models\Order;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Schema;
use Tests\TestCase;

class CompanySchemaTest extends TestCase
{
    use RefreshDatabase;
    /**
     * @return void
     */
    public function test_database_has_expected_columns(): void
    {
        $this->assertTrue(
            Schema::hasColumns('companies', [
                'id',
                'name',
                'email',
                'phone',
                'address',
                'country_id',
            ])
        );
    }

    /**
     *
     * @return void
     */
    public function test_model_can_be_instantiated(): void
    {
        $company = Company::factory()->create();

        $this->assertModelExists($company);
    }

    /**
     * @return void
     */
    public function test_company_belongs_to_country(): void
    {
        $country = Country::factory()->create();
        $company = Company::factory()
            ->for($country)
            ->create();

        $this->assertInstanceOf(Country::class, $company->country);
    }

    /**
     * @return void
     */
    public function test_company_has_sales(): void
    {
        $company = Company::factory()
            ->hasSales(3)
            ->create();

        $this->assertEquals(3, $company->sales->count());

        $this->assertInstanceOf(Collection::class, $company->sales);
    }

    /**
     * @return void
     */
    public function test_company_has_purchases(): void
    {
        $company = Company::factory()
            ->hasPurchases(3)
            ->create();

        $this->assertEquals(3, $company->purchases->count());

        $this->assertInstanceOf(Collection::class, $company->purchases);
    }

    /**
     * @return void
     */
    public function test_company_can_get_country_name(): void
    {
        $country = Country::factory()->create();
        $company = Company::factory()
            ->for($country)
            ->create();

        $this->assertEquals($country->name, $company->country->name);
    }

    /**
     * @return void
     */
    public function test_scope_is_payer(): void
    {
        $c1 = Company::factory()->create();

        $this->assertEquals(0, Company::isPayer()->count());

        Order::factory()
            ->for($c1, 'purchaser')
            ->create(['purchaser_pays' => true]);

        $this->assertEquals(1, Company::isPayer()->count());
        $this->assertEquals($c1->id, Company::isPayer()->first()->id);

        $c2 = Company::factory()->create();

        $this->assertEquals(1, Company::isPayer()->count());

        Order::factory()
            ->for($c2, 'purchaser')
            ->create(['purchaser_pays' => false]);

        $this->assertEquals(1, Company::isPayer()->count());


        Order::factory()
            ->for($c2, 'purchaser')
           ->create(['purchaser_pays' => true]);

        $this->assertEquals(2, Company::isPayer()->count());
        $this->assertEquals([
            $c1->id,
            $c2->id
        ], Company::isPayer()->orderBy('id')->get()->pluck('id')->toArray());

    }
}
