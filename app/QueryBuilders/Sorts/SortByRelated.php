<?php

namespace App\QueryBuilders\Sorts;

use Illuminate\Database\Eloquent\Builder;
use Spatie\QueryBuilder\Sorts\Sort;

class SortByRelated implements Sort
{
    /**
     * @param Builder $query
     * @param bool $descending
     * @param string $property
     * @return void
     */
    public function __invoke(Builder $query, bool $descending, string $property): void
    {
        $model = $query->getModel();

        [
            $relationship,
            $column
        ] = explode('.', $property);

        if ($column === null) {
            $column = 'name';
        }

        $foreign_model = $model->$relationship()->getModel();
        $foreign_key = $model->$relationship()->getQualifiedForeignKeyName();
        $owner_key = $model->$relationship()->getQualifiedOwnerKeyName();

        $query->orderBy($foreign_model::select($column)->whereColumn($owner_key, $foreign_key), $descending ? 'desc' : 'asc');
    }
}
