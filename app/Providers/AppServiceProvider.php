<?php

namespace App\Providers;

use App\Contracts\InvoiceNumberContract;
use App\Services\InvoiceNumberService;
use Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider;
use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register(): void
    {
        if ($this->app->isLocal()) {
            $this->app->register(IdeHelperServiceProvider::class);
        }
        $this->app->singleton(InvoiceNumberContract::class, InvoiceNumberService::class);

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(): void
    {

        ResetPassword::createUrlUsing(function ($user, string $token) {
            return env('SPA_URL') . '/login/reset-password?token=' . $token;
        });
    }
}
