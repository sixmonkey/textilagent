<?php

namespace App\Contracts;

use App\Models\Invoice;

interface InvoiceNumberContract
{
    public function getInvoiceNumber(Invoice $invoice): string;
}
