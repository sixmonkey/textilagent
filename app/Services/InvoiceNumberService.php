<?php

namespace App\Services;

use App\Contracts\InvoiceNumberContract;
use App\Models\Invoice;
use Illuminate\Support\Str;

class InvoiceNumberService implements InvoiceNumberContract
{

    public function getInvoiceNumber(Invoice $invoice): string
    {
        $date = $invoice->date;

        $latestInvoiceInMonth = Invoice::whereMonth('date', $invoice->date)
            ->whereYear('date', $invoice->date)
            ->latest('invoice_number')
            ->first();

        $invoiceNumber = (int)Str::afterLast($latestInvoiceInMonth?->invoice_number, '-');
        $invoiceNumber++;

        return sprintf('%02d-%02d-%04d', $date->year, $date->month, $invoiceNumber);
    }
}
