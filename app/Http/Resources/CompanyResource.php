<?php

namespace App\Http\Resources;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class CompanyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'title' => $this->title,
            'email' => $this->email,
            'address' => $this->address,
            'phone' => $this->phone,

            'country' => new CountryResource($this->whenLoaded('country')),
            'orders' => new OrderResource($this->whenLoaded('orders')),
            'purchases' => new OrderResource($this->whenLoaded('purchases')),
        ];
    }
}
