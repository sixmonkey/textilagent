<?php

namespace App\Http\Controllers;

use App\Http\Requests\Request;
use App\Models\User;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Str;
use Spatie\MediaLibrary\Support\MediaStream;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\AllowedInclude;
use Spatie\QueryBuilder\AllowedSort;
use Spatie\QueryBuilder\QueryBuilder;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Response;
use ZipStream\Option\Archive;

class Controller extends BaseController
{
    use AuthorizesRequests;
    use DispatchesJobs;
    use ValidatesRequests;

    /**
     * The model for this resource
     *
     * @var string
     */
    public string $model = User::class;

    /**
     * The resource for this resource
     *
     * @var string
     */
    public string $resource = JsonResource::class;

    /**
     * allowed sort parameters
     *
     * @var array
     */
    protected array $allowed_sorts = [];

    /**
     * allowed sort parameters
     *
     * @var array
     */
    protected array $allowed_custom_sorts = [];

    /**
     * allowed filters
     *
     * @var array
     */
    protected array $allowed_filters = [];

    /**
     * allowed scopes
     *
     * @var array
     */
    protected array $allowed_filter_scopes = [];

    /**
     * allowed includes
     *
     * @var array
     */
    protected array $allowed_includes = [];
    /**
     * allowed includes
     *
     * @var array
     */
    protected array $allowed_counts = [];

    /**
     * allowed fields
     *
     * @var array
     */
    protected array $allowed_fields = [];

    /**
     * equivalent of good old CakePHP beforeEach
     *
     * @param $method
     * @param $parameters
     * @return Response|JsonResource|MediaStream
     */
    public function callAction($method, $parameters): Response|JsonResource|MediaStream|int
    {
        $this->allowed_includes = collect($this->allowed_includes)
            ->map(function ($item) {
                return is_string($item) ? Str::camel($item) : $item;
            })
            ->toArray();

        return parent::callAction($method, $parameters);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return AnonymousResourceCollection
     * @throws AuthorizationException
     */
    public function index(Request $request): AnonymousResourceCollection
    {
        $this->authorize('viewAny', $this->model);

        $allowed_filters = [];
        if (method_exists($this->model, 'scopeSearch')) {
            $allowed_filters[] = AllowedFilter::scope('search');
        }

        foreach ($this->allowed_filters as $filter) {
            $allowed_filters[] = AllowedFilter::exact($filter);
        }

        foreach ($this->allowed_filter_scopes as $scope) {
            $allowed_filters[] = AllowedFilter::scope($scope);
        }
        $allowed_filters[] = AllowedFilter::scope('has');

        foreach ($this->allowed_filter_scopes as $count) {
            $this->allowed_includes[] = AllowedInclude::count($count);
        }

        foreach ($this->allowed_custom_sorts as $alias => $sort) {
            $this->allowed_sorts[] = AllowedSort::custom($alias, new $sort['class'], $sort['column']);
        }

        $result = QueryBuilder::for($this->model)
            ->allowedSorts($this->allowed_sorts)
            ->allowedFilters($allowed_filters)
            ->allowedFields($this->allowed_fields)
            ->allowedIncludes($this->allowed_includes);


        return call_user_func([
            $this->resource,
            'collection'
        ],
            $request->has('page') ? $result->jsonPaginate() : $result->get()
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     * @throws AuthorizationException
     */
    public function store(Request $request): JsonResponse
    {
        $this->authorize('create', $this->model);

        $entity = new $this->model();

        $new = call_user_func([
            $this->model,
            'create'
        ], $request->validated());

        $this->storeRelated($new, $request->validated());

        $result = QueryBuilder::for($this->model)
            ->allowedSorts($this->allowed_sorts)
            ->allowedFields($this->allowed_fields)
            ->allowedIncludes($this->allowed_includes)
            ->find($new->id);

        return (new $this->resource(
            $result
        ))
            ->toResponse($request)
            ->setStatusCode(201);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return JsonResource
     * @throws AuthorizationException
     */
    public function show(int $id): JsonResource
    {
        $result = QueryBuilder::for($this->model)
            ->allowedSorts($this->allowed_sorts)
            ->allowedFields($this->allowed_fields)
            ->allowedIncludes($this->allowed_includes)
            ->findOrFail($id);

        $this->authorize('view', $result);

        return new $this->resource(
            $result
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return object
     * @throws AuthorizationException
     */
    public function update(Request $request, int $id): object
    {
        $entity = call_user_func([
            $this->model,
            'findOrFail'
        ], $id);

        $this->authorize('update', $entity);
        $entity->update($request->validated());

        $this->storeRelated($entity, $request->validated());

        return new $this->resource(
            QueryBuilder::for($this->model)
                ->allowedSorts($this->allowed_sorts)
                ->allowedFields($this->allowed_fields)
                ->allowedIncludes($this->allowed_includes)
                ->find($entity->id)
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return JsonResponse
     * @throws AuthorizationException
     */
    public function destroy(int $id): JsonResponse
    {
        $entity = call_user_func([
            $this->model,
            'findOrFail'
        ], $id);

        $this->authorize('delete', $entity);

        if ($entity->delete()) {
            return new JsonResponse(['message' => 'Successfully deleted ' . ($entity->title ?? '#' . $entity->id) . '!']);
        }
        return new JsonResponse(['message' => 'Unknown error'], 500);
    }

    /**
     * list related documents
     *
     * @param int $id
     * @return MediaStream|JsonResponse
     * @throws AuthorizationException
     */
    public function documents_index(int $id): MediaStream|JsonResponse
    {
        $this->authorize('viewAny', $this->model);

        $item = call_user_func([
            $this->model,
            'findOrFail'
        ], $id);
        $documents = $item->getMedia('documents');

        if (request()->has('download')) {

            return MediaStream::create(date('Y-m-d_') . Str::slug($item->title) . '.zip')
                ->useZipOptions(function (Archive $zipOptions) {
                    $zipOptions->setZeroHeader(true);
                })
                ->addMedia($documents);
        }

        return new JsonResponse($documents->toArray(), 200);
    }

    /**
     * show related document
     *
     * @param int $id
     * @param int $document_id
     * @return BinaryFileResponse|JsonResponse
     * @throws AuthorizationException
     */
    public function documents_show(int $id, int $document_id): BinaryFileResponse|JsonResponse
    {
        $this->authorize('viewAny', $this->model);

        $item = call_user_func([
            $this->model,
            'findOrFail'
        ], $id)
            ->getMedia('documents')
            ->where('id', $document_id)
            ->first();

        if (request()->has('download')) {
            return response()->download($item->getPath(), $item->file_name);
        }

        return new JsonResponse($item->toArray(), 200);
    }

    /**
     * store related documents
     * @param int $id
     * @param Request $request
     * @return JsonResponse
     * @throws AuthorizationException
     */
    public function documents_store(int $id, Request $request): JsonResponse
    {
        $entity = call_user_func([
            $this->model,
            'findOrFail'
        ], $id);

        $this->authorize('update', $entity);

        $entity
            ->addMultipleMediaFromRequest(['documents'])
            ->each(function ($fileAdder) {
                $fileAdder->toMediaCollection('documents');
            });

        return new JsonResponse(['message' => 'Successfully uploaded documents!']);
    }

    /**
     * delete related document
     *
     * @param int $id
     * @param int $document_id
     * @return JsonResponse
     * @throws AuthorizationException
     */
    public function documents_delete(int $id, int $document_id): JsonResponse
    {
        $entity = call_user_func([
            $this->model,
            'findOrFail'
        ], $id);

        $this->authorize('update', $entity);

        $entity
            ->getMedia('documents')
            ->where('id', $document_id)
            ->first()
            ->delete();

        return new JsonResponse(['message' => 'Successfully deleted document!']);
    }

    /**
     * Store related from request
     *
     * @param $for
     * @param $request
     * @return void
     */
    private function storeRelated($for, $request): void
    {
        if (!method_exists($for, 'definedRelationships')) {
            return;
        }

        $request = collect($request);

        foreach (call_user_func([
            $for,
            'definedRelationships'
        ]) as $relationship => $type) {
            $relationship_key = Str::snake($relationship);

            if ($request->has($relationship_key)) {
                $data = $request->get($relationship_key);

                if (!is_array($data)) {
                    continue;
                }
                switch ($type) {
                    case 'BelongsTo':
                        $this->storeBelongsTo($for, $relationship, $data);
                        break;
                    case 'HasMany':
                        $this->storeHasMany($for, $relationship, $data);
                        break;
                }
                $for->save();
            }
        }
    }

    /**
     * @param $for
     * @param $relationship
     * @param $data
     * @return void
     */
    private function storeBelongsTo($for, $relationship, $data): void
    {
        $related = call_user_func([
            $for,
            $relationship
        ])
            ->getRelated()
            ->where('id', $data['id'] ?? null) // TODO make sure id exists when set
            ->firstOr(function () use ($for, $relationship, $data) {
                return call_user_func([
                    $for,
                    $relationship
                ])
                    ->getRelated()->create($data);
            });
        $this->storeRelated($related, collect($data));
        call_user_func([
            $for,
            $relationship
        ])->associate($related);
    }

    /**
     * @param $for
     * @param $relationship
     * @param $data
     * @return void
     */
    private function storeHasMany($for, $relationship, $data): void
    {
        $newItems = [];
        $related = call_user_func([
            $for,
            $relationship
        ])->getRelated();
        collect($data)->each(function ($item) use ($for, $relationship, $related, &$newItems) {
            $newItem = call_user_func([
                $related,
                'where'
            ], ['id' => $item['id'] ?? null])
                ->firstOr(function () use ($for, $relationship, $item) {
                    return call_user_func([
                        $for,
                        $relationship
                    ])
                        ->getRelated()
                        ->create($item);
                });

            $newItem->fill($item);
            $newItems[] = $newItem;

            $this->storeRelated($newItem, collect($item));
        });
        call_user_func([
            $for,
            $relationship
        ])->whereNotIn('id', collect($newItems)->pluck('id'))->delete();
        call_user_func([
            $for,
            $relationship
        ])->saveMany($newItems);
        $for->touch();
    }
}
