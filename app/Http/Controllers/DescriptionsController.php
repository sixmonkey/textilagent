<?php

namespace App\Http\Controllers;

use App\Models\Description;
use App\Models\Typology;

class DescriptionsController extends Controller
{
    /**
     * The model for this resource
     *
     * @var string
     */
    public string $model = Description::class;

}
