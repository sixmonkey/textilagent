<?php

namespace App\Http\Controllers;

use App\Http\Resources\UserResource;
use App\Models\User;

class UsersController extends Controller
{
    /**
     * The model for this resource
     *
     * @var string
     */
    public string $model = User::class;

    /**
     * The resource for this controller
     *
     * @var string
     */
    public string $resource = UserResource::class;

    /**
     * allowed sort parameters
     *
     * @var array
     */
    protected array $allowed_sorts = ['name'];

    /**
     * allowed filters
     *
     * @var array
     */
    protected array $allowed_filters = ['admin', 'country_id'];

    /**
     * allowed filters
     *
     * @var array
     */
    protected array $allowed_fields = ['name', 'orders.contract'];

    /**
     * allowed includes
     *
     * @var array
     */
    protected array $allowed_includes = ['orders', 'country'];
}
