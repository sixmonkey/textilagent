<?php

namespace App\Http\Controllers;

use App\Models\Typology;

class TypologiesController extends Controller
{
    /**
     * The model for this resource
     *
     * @var string
     */
    public string $model = Typology::class;

    /**
     * allowed sort parameters
     *
     * @var array
     */
    protected array $allowed_sorts = [
        'code'
    ];

}
