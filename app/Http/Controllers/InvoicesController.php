<?php

namespace App\Http\Controllers;

use App\Models\Currency;
use App\Models\Invoice;
use App\QueryBuilders\Sorts\SortByRelated;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Http\Response;

class InvoicesController extends Controller
{
    /**
     * The model for this resource
     *
     * @var string
     */
    public string $model = Invoice::class;

    /**
     * allowed sort parameters
     *
     * @var array
     */
    protected array $allowed_sorts = [
        'date',
        'due_date',
        'invoice_number',
        'paid',
    ];

    /**
     * allowed custom sort parameters
     *
     * @var array
     */
    protected array $allowed_custom_sorts = [
        'receiver' => [
            'class'  => SortByRelated::class,
            'column' => 'receiver.name',
        ]
    ];

    /**
     * allowed fields
     *
     * @var array
     */
    protected array $allowed_fields = [
        'id',
        'date',
        'due_date',
        'invoice_number',
    ];

    /**
     * allowed includes of relationships
     *
     * @var array|string[]
     */
    protected array $allowed_includes = [
        'shipments',
        'receiver',
    ];

    /**
     * allowed filters
     *
     * @var array
     */
    protected array $allowed_filters = [
        'receiver_id',
        'paid',
    ];

    /**
     * allowed filters
     *
     * @var array
     */
    protected array $allowed_filter_scopes = [
        'date_between',
    ];

    /**
     * Download the invoice as a pdf
     *
     * @param int $id
     * @return Response
     */
    public function pdf(int $id): Response
    {
        $defaultCurrency = Currency::where('default', true)->firstOrFail();
        $currency = request()->has('currency_id') ? Currency::findOrFail(request()->get('currency_id')) : $defaultCurrency;

        $invoice = Invoice::with('shipments', 'receiver')->findOrFail($id);
        $invoice->shipments->map(function ($shipment) use ($currency, $defaultCurrency, $invoice) {
            $shipment->total_commission = \AmrShawky\LaravelCurrency\Facade\Currency::convert()
                ->from($defaultCurrency->code)
                ->to($currency->code)
                ->amount($shipment->total_commission)
                ->date($invoice->date->format('Y-m-d'))
                ->round(2)
                ->get();
        });

        return Pdf::loadView('pdf.invoice', compact('invoice', 'currency'))->stream('invoice.pdf');
    }
}
