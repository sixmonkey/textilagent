<?php

namespace App\Http\Controllers;

use App\Http\Resources\CompanyResource;
use App\Models\Company;

class CompaniesController extends Controller
{
    /**
     * The model for this resource
     *
     * @var string
     */
    public string $model = Company::class;

    /**
     * The resource name
     *
     * @var string
     */
    public string $resource = CompanyResource::class;

    /**
     * allowed sort parameters
     *
     * @var array
     */
    protected array $allowed_sorts = ['name'];

    /**
     * allowed filters
     *
     * @var array
     */
    protected array $allowed_filters = ['country_id'];

    /**
     * @var array
     */
    protected array $allowed_filter_scopes = [
        'is_payer'
    ];

    /**
     * allowed filters
     *
     * @var array
     */
    protected array $allowed_fields = [
        'id',
        'name',
        'countries.name'
    ];

    /**
     * allowed includes
     *
     * @var array
     */
    protected array $allowed_includes = ['sales', 'purchases', 'country'];
}
