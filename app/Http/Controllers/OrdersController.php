<?php

namespace App\Http\Controllers;

use App\Http\Resources\OrderResource;
use App\Models\Order;
use App\QueryBuilders\Sorts\SortByRelated;

class OrdersController extends Controller
{
    /**
     * The model for this resource
     *
     * @var string
     */
    public string $model = Order::class;

    /**
     * The resource name
     *
     * @var string
     */
    public string $resource = OrderResource::class;

    /**
     * allowed sort parameters
     *
     * @var array
     */
    protected array $allowed_sorts = [
        'date',
        'completed',
        'payed',
        'purchaser_pays',
        'contract',
        'customer_order_number',
    ];

    /**
     * allowed custom sort parameters
     *
     * @var array
     */
    protected array $allowed_custom_sorts = [
        'agent'    => [
            'class'  => SortByRelated::class,
            'column' => 'agent.name',
        ],
        'seller'    => [
            'class'  => SortByRelated::class,
            'column' => 'seller.name',
        ],
        'purchaser' => [
            'class'  => SortByRelated::class,
            'column' => 'purchaser.name',
        ]
    ];

    /**
     * allowed fields
     *
     * @var array
     */
    protected array $allowed_fields = [
        'id',
        'title',
        'order_items.id',
        'order_items.title',
        'order_items.unit',
    ];

    /**
     * allowed includes of relationships
     *
     * @var array|string[]
     */
    protected array $allowed_includes = [
        'currency',
        'agent',
        'seller',
        'purchaser',
        'order_items',
        'order_items.unit',
        'shipments',
        'shipment_items',
        'sub_agents',
        'sub_agents.user'
    ];

    /**
     * allowed filters
     *
     * @var array
     */
    protected array $allowed_filters = [
        'agent_id',
        'seller_id',
        'purchaser_id',
        'purchaser_pays',
        'completed',
        'payed',
        'agent_id',
    ];

    /**
     * allowed filters
     *
     * @var array
     */
    protected array $allowed_filter_scopes = [
        'date_between',
    ];
}
