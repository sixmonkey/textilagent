<?php

namespace App\Http\Controllers;

use App\Http\Resources\OrderResource;
use App\Models\Order;
use App\Models\OrderItem;
use App\QueryBuilders\Sorts\SortByRelated;

class OrderItemsController extends Controller
{
    /**
     * The model for this resource
     *
     * @var string
     */
    public string $model = OrderItem::class;

    /**
     * allowed sort parameters
     *
     * @var array
     */
    protected array $allowed_sorts = [
        'etd',
    ];

    /**
     * allowed fields
     *
     * @var array
     */
    protected array $allowed_fields = [
        'id',
        'etd',
    ];

    /**
     * allowed includes of relationships
     *
     * @var array|string[]
     */
    protected array $allowed_includes = [
        'order',
        'order.seller',
        'order.purchaser',
    ];
}
