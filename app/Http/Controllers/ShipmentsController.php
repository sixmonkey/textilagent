<?php

namespace App\Http\Controllers;

use App\Http\Resources\ShipmentResource;
use App\Models\Shipment;
use App\QueryBuilders\Sorts\SortByRelated;

class ShipmentsController extends Controller
{
    /**
     * The model for this resource
     *
     * @var string
     */
    public string $model = Shipment::class;

    /**
     * The resource name
     *
     * @var string
     */
    public string $resource = ShipmentResource::class;

    /**
     * allowed sort parameters
     *
     * @var array
     */
    protected array $allowed_sorts = [
        'date',
        'invoice',
        'payed'
    ];


    /**
     * allowed custom sort parameters
     *
     * @var array
     */
    protected array $allowed_custom_sorts = [
        'seller'    => [
            'class'  => SortByRelated::class,
            'column' => 'seller.name',
        ],
        'purchaser' => [
            'class'  => SortByRelated::class,
            'column' => 'purchaser.name',
        ]
    ];

    /**
     * allowed fields
     *
     * @var array
     */
    protected array $allowed_fields = [
        'id',
        'date',
        'total_commission',
        'payed',
        'seller_id',
        'purchaser_id',
        'invoice',
    ];

    /**
     * allowed includes of relationships
     *
     * @var array|string[]
     */
    protected array $allowed_includes = [
        'shipment_items',
        'shipment_items.order_item',
        'shipment_items.order_item.order',
        'seller',
        'purchaser',
        'orders',
        'invoice',
    ];

    /**
     * allowed filters
     *
     * @var array
     */
    protected array $allowed_filter_scopes = [
        'order_id',
        'date_between',
        'payer',
        'invoice_or_null',
    ];

    /**
     * allowed filters
     *
     * @var array
     */
    protected array $allowed_filters = [
        'seller_id',
        'purchaser_id',
        'invoice_id',
    ];
}
