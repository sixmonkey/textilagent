<?php

namespace App\Http\Controllers;

use App\Models\Country;

class CountriesController extends Controller
{
    /**
     * The model for this resource
     *
     * @var string
     */
    public string $model = Country::class;
}
