<?php

namespace App\Http\Middleware;

use Illuminate\Http\Request;
use Closure;

class NotFoundWhenProduction
{

    public function handle(Request $request, Closure $next)
    {
        if (app()->environment('production')) {
            abort(404);
        }

        return $next($request);
    }
}
