<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DocumentsStoreShipmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'documents' => ['required', 'array'],
            'documents.*' => ['required', 'mime_types:application/pdf'],
        ];
    }

    /**
     * Get the messages that apply to the request.
     *
     * @return array
     */
    public function messages(): array
    {
        return [
            'documents.required' => 'Please select a file to upload.',
            'documents.array' => 'Please select a file to upload.',
            'documents.*.required' => 'Please select a file to upload.',
            'documents.*.mime_types' => 'Please upload a PDF file.',
        ];
    }
}
