<?php

namespace App\Http\Requests;

use BrokeYourBike\MoneyValidation\IsValidCurrency;
use Illuminate\Foundation\Http\FormRequest;

class StoreOrderRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'contract'              => [
                'required',
                'min:3',
                'max:32'
            ],
            'customer_order_number' => [
                'nullable',
                'min:3',
                'max:32'
            ],
            'date'                  => [
                'required',
                'date'
            ],
            'purchaser_pays'        => ['boolean'],

            'seller'      => ['required'],
            'seller.id'   => [
                'exclude_if:seller.id,null',
                'exists:companies,id',
                'different:purchaser.id'
            ],
            'seller.name' => [
                'exclude_unless:seller.id,null',
                'required',
                'min:3',
                'max:255',
                'different:purchaser.name'
            ],

            'purchaser'      => ['required'],
            'purchaser.id'   => [
                'exclude_if:purchaser.id,null',
                'exists:companies,id',
                'different:seller.id'
            ],
            'purchaser.name' => [
                'exclude_unless:purchaser.id,null',
                'required',
                'min:3',
                'max:255',
                'different:seller.name'
            ],

            'currency'      => ['required'],
            'currency.id'   => [
                'exclude_if:currency.id,null',
                'exists:currencies,id'
            ],
            'currency.code' => [
                'exclude_unless:currency.id,null',
                'required',
                'size:3',
                new IsValidCurrency()
            ],

            'agent'      => ['required'],
            'agent.id'   => [
                'exclude_if:agent.id,null',
                'exists:users,id'
            ],
            'agent.name' => [
                'exclude_unless:agent.id,null',
                'required',
                'min:3',
                'max:255',
                'unique:users,name'
            ],
        ];
    }
}
