<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreShipmentRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'shipment_number' => 'required|max:32',
            'date' => 'required|date',
            'seller' => 'required',
            'seller.id' => 'exists:companies,id|different:purchaser.id',
            'purchaser' => 'required',
            'purchaser.id' => 'exists:companies,id|different:seller.id',
            'purchaser.name' => 'exclude_unless:purchaser.id,null|required|min:3|max:255|different:seller.name',
            'shipment_items' => 'sometimes|array',
            'shipment_items.*.amount' => 'required|numeric|min:1',
            'shipment_items.*.order_item.id' => 'required|exists:shipment_items,id',
        ];
    }

    /**
     * @return string[]
     */
    public function messages(): array
    {
        return [
            'shipment_items.required' => 'Please give at least one shipped item'
        ];
    }
}
