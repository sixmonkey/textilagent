<?php

namespace App\Http\Requests;

class UpdateShipmentRequest extends StoreShipmentRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'shipment_number' => 'sometimes|min:3|max:25',
            'date' => 'date',

            'payed' => 'boolean',

            'seller' => 'required',
            'seller.id' => 'sometimes|exclude_if:seller.id,null|exists:companies,id|different:purchaser.id',
            'seller.name' => 'sometimes|exclude_unless:seller.id,null|required|min:3|max:255|different:purchaser.name',

            'purchaser' => 'required',
            'purchaser.id' => 'sometimes|exclude_if:purchaser.id,null|exists:companies,id|different:seller.id',
            'purchaser.name' => 'sometimes|exclude_unless:seller.id,null|required|min:3|max:255|different:seller.name',

            'shipment_items' => 'array|min:1',
            'shipment_items.*.amount' => 'required|numeric',
            'shipment_items.*.order_item.id' => 'required|exists:order_items,id',
        ];
    }
}
