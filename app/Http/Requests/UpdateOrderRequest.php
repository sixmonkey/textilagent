<?php

namespace App\Http\Requests;

use BrokeYourBike\MoneyValidation\IsValidCurrency;

class UpdateOrderRequest extends StoreOrderRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'contract' => [
                'required',
                'min:3',
                'max:32'
            ],
            'customer_order_number' => [
                'max:32'
            ],
            'date'     => [
                'required',
                'date'
            ],
            'purchaser_pays' => [
                'required',
                'boolean'
            ],
            'completed' => [
                'required',
                'boolean'
            ],

            'currency.id'   => [
                'exclude_if:currency.id,null',
                'exists:currencies,id'
            ],
            'currency.code' => [
                'exclude_unless:currency.id,null',
                'required',
                'size:3',
                new IsValidCurrency()
            ],

            'seller.id'      => [
                'exclude_if:seller.id,null',
                'exists:companies,id',
                'different:purchaser.id'
            ],
            'seller.name'    => [
                'exclude_unless:seller.id,null',
                'required',
                'min:3',
                'max:255',
                'different:purchaser.name'
            ],
            'purchaser.id'   => [
                'exclude_if:purchaser.id,null',
                'exists:companies,id',
                'different:seller.id'
            ],
            'purchaser.name' => [
                'exclude_unless:purchaser.id,null',
                'required',
                'min:3',
                'max:255',
                'different:seller.name'
            ],

            'agent.id'   => [
                'exclude_if:agent.id,null',
                'exists:users,id'
            ],
            'agent.name' => [
                'exclude_unless:agent.id,null',
                'required',
                'min:3',
                'max:255',
                'unique:users,name'
            ],

            'order_items'             => [
                'required',
                'array',
                'min:1'
            ],
            'order_items.*.typology'  => [
                'required'
            ],
            'order_items.*.amount'    => [
                'required',
                'numeric',
                'min:1'
            ],
            'order_items.*.price'     => [
                'required',
                'numeric',
                'min:0.01'
            ],
            'order_items.*.provision' => [
                'required',
                'numeric'
            ],

            'order_items.*.unit.id'     => [
                'exclude_if:order_items.*.unit.id,null',
                'exists:units,id'
            ],
            'order_items.*.unit.code'   => [
                'exclude_unless:order_items.*.unit.id,null',
                'required',
                'min:2',
                'max:5',
                'unique:units,code'
            ],
            'order_items.*.etd'         => [
                'required',
                'date'
            ],
            'order_items.*.description' => [
                'required'
            ],

            'sub_agents'             => [
                'sometimes',
                'array'
            ],
            'sub_agents.*.cut'       => [
                'required',
                'numeric'
            ],
            'sub_agents.*.user'      => ['required'],
            'sub_agents.*.user.id'   => [
                'exclude_unless:sub_agents.*.user.name,null',
                'required',
                'exists:users,id',
                'different:agent.id'
            ],
            'sub_agents.*.user.name' => [
                'exclude_unless:sub_agents.*.user.id,null',
                'required'
            ],
        ];
    }
}
