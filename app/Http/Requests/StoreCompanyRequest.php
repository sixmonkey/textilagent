<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreCompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name'       => [
                'required',
                Rule::unique('companies', 'name')->ignore($this->company),
                'string',
                'min:3',
                'max:255'
            ],
            'email'      => [
                'sometimes',
                'nullable',
                Rule::unique('companies', 'email')->ignore($this->company),
                'email',
            ],
            'address'    => '',
            'phone'      => '',
            'country.id' => 'exists:countries,id',
        ];
    }
}
