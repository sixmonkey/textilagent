<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreInvoiceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'date'           => 'required|date',
            'due_date'       => 'required|date',
            'receiver.id'    => 'required|integer|exists:companies,id',
            'paid'           => 'sometimes|boolean',
            'invoice_number' => 'sometimes|string|min:3|max:25|unique:invoices,invoice_number,' . $this->id,
        ];
    }
}
