<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Stephenjude\DefaultModelSorting\Traits\DefaultOrderBy;

class Description extends Model
{
    use HasFactory;
    use DefaultOrderBy;

    /**
     * the table associated with the model
     *
     * @var string
     */
    protected $table = 'order_items';

    /**
     * the default sort order column
     *
     * @var string
     */
    protected static string $orderByColumn = 'description';


    /**
     * the default sort order direction
     *
     * @var string
     */
    protected static string $orderByColumnDirection = 'desc';

    /**
     * Get a new query builder for the model's table.
     *
     * @return Builder
     */
    public function newQuery(): Builder
    {
        $query = parent::newQuery();
        $query->select('description as name');
        return $query->groupBy($this->table . '.description');
    }
}
