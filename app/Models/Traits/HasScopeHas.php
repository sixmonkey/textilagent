<?php

namespace App\Models\Traits;

use Illuminate\Database\Eloquent\Builder;

trait HasScopeHas
{
    public function scopeHas(Builder $query, string $relations): Builder
    {
        $relations = explode(',', $relations);
        foreach ($relations as $relation) {
            $query->whereHas($relation);
        }
        return $query;
    }
}
