<?php

namespace App\Models\Traits;

trait HasDownloads
{

    /**
     * The attributes that should be appended for serialization.
     *
     */
    public function initializeHasDownloads(): void
    {
        $this->appends[] = 'download_url';
        $this->appends[] = 'has_downloads';
    }

    /**
     * the related download link
     *
     * @return string|null
     */
    public function getDownloadUrlAttribute(): ?string
    {
        return $this->media()->count() > 0 ? (string)url(route('shipments.documents.index', [$this->id]) . '?download') : null;
    }

    /**
     * the related download link
     *
     * @return bool
     */
    public function getHasDownloadsAttribute(): bool
    {
        return $this->media()->count() > 0;
    }
}
