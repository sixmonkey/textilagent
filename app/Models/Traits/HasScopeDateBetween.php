<?php

namespace App\Models\Traits;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Carbon;

trait HasScopeDateBetween
{

    /**
     * @param Builder $query
     * @param $start
     * @param $end
     * @return Builder
     */
    public function scopeDateBetween(Builder $query, $start, $end = null): Builder
    {
        $start = Carbon::parse($start);
        $end = $end ? Carbon::parse($end) : $start->copy();

        $range = [$start, $end->endOfMonth()];
        sort($range);
        return $query->whereBetween('date', $range);
    }
}
