<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Stephenjude\DefaultModelSorting\Traits\DefaultOrderBy;

class Typology extends Model
{
    use HasFactory;
    use DefaultOrderBy;

    /**
     * the table associated with the model
     *
     * @var string
     */
    protected $table = 'order_items';

    /**
     * the default sort order column
     *
     * @var string
     */
    protected static string $orderByColumn = 'name';


    /**
     * the default sort order direction
     *
     * @var string
     */
    protected static string $orderByColumnDirection = 'desc';

    /**
     * Get a new query builder for the model's table.
     *
     * @return Builder
     */
    public function newQuery(): Builder
    {
        $query = parent::newQuery();
        $query->select('typology as name');
        return $query->groupBy($this->table . '.typology');
    }
}
