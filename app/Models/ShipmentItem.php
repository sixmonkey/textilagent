<?php

namespace App\Models;

use App\Models\Traits\HasRelationships;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ShipmentItem extends Model
{
    use HasFactory;
    use HasRelationships;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<string>
     */
    protected $fillable = [
        'amount'
    ];

    /**
     * Get the amount that is left to ship
     *
     * @var string[]
     */
    protected $appends = [
        'amount_left',
        'amount_original'
    ];

    /**
     * @return void
     */
    protected static function booted(): void
    {
        static::saved(function ($model) {
           $model->shipment?->touch();
        });
    }

    /**
     * the related order item
     *
     * @return BelongsTo
     */
    public function orderItem(): BelongsTo
    {
        return $this->belongsTo(OrderItem::class);
    }

    /**
     * the related shipment
     *
     * @return BelongsTo
     */
    public function shipment(): BelongsTo
    {
        return $this->belongsTo(Shipment::class);
    }

    /**
     * @param $value
     * @return int
     */
    public function getAmountLeftAttribute($value): int
    {
        return intval($this->orderItem()->first()->amount + $this->amount);
    }

    /**
     * @param $value
     * @return int
     */
    public function getAmountOriginalAttribute($value): int
    {
        return intval($this->amount);
    }
}
