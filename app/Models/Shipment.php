<?php

namespace App\Models;

use App\Models\Traits\HasDownloads;
use App\Models\Traits\HasScopeDateBetween;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Staudenmeir\EloquentHasManyDeep\HasManyDeep;
use Staudenmeir\EloquentHasManyDeep\HasRelationships;
use Stephenjude\DefaultModelSorting\Traits\DefaultOrderBy;
use Znck\Eloquent\Traits\BelongsToThrough;
use \AmrShawky\LaravelCurrency\Facade\Currency as LaravelCurrency;

class Shipment extends Model implements HasMedia
{
    use HasFactory;
    use HasRelationships;
    use Traits\HasRelationships;
    use BelongsToThrough;
    use DefaultOrderBy;
    use InteractsWithMedia;
    use HasScopeDateBetween;
    use HasDownloads;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<string>
     */
    protected $fillable = [
        'shipment_number',
        'date',
        'payed'
    ];

    /**
     * the default sort order column
     *
     * @var string
     */
    protected static string $orderByColumn = 'date';


    /**
     * the default sort order direction
     *
     * @var string
     */
    protected static string $orderByColumnDirection = 'desc';

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'date' => 'datetime:Y-m-d',
        'payed' => 'boolean',
        'total_commission' => 'float',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var string[]
     */
    protected $appends = [
        'title',
        'currency',
    ];

    protected static function booted(): void
    {
        static::updating(function (Shipment $shipment) {
            $defaultCurrency = $shipment->getCurrencyAttribute();

            $total = 0;
            $shipment->shipmentItems->each(function (ShipmentItem $shipmentItem) use (&$total, $defaultCurrency) {
                $price = $shipmentItem->orderItem->order->currency->code !== $defaultCurrency ?
                    LaravelCurrency::convert()
                        ->from($shipmentItem->orderItem->order->currency->code)
                        ->to($defaultCurrency->code)
                        ->amount($shipmentItem->orderItem->price)
                        ->date($shipmentItem->orderItem->order->date->format('Y-m-d'))
                        ->get()
                    :
                    $shipmentItem->orderItem->price;

                $total += $shipmentItem->amount * $price * $shipmentItem->orderItem->provision / 100;
            });
            $shipment->total_commission = $total;
        });
    }

    /**
     * related shipment items
     *
     * @return HasMany
     */
    public function shipmentItems(): HasMany
    {
        return $this->hasMany(ShipmentItem::class);
    }

    /**
     * the related order items
     *
     * @return BelongsToMany
     */
    public function orderItems(): BelongsToMany
    {
        return $this->belongsToMany(OrderItem::class, 'shipment_items');
    }

    /**
     * the related invoice
     *
     * @return BelongsTo
     */
    public function invoice(): BelongsTo
    {
        return $this->belongsTo(Invoice::class);
    }

    /**
     * the related orders
     *
     * @return HasManyDeep
     */
    public function orders(): HasManyDeep
    {
        return $this
            ->hasManyDeepFromRelations(
                $this->orderItems(),
                (new OrderItem())->order()
            )
            ->groupBy('orders.id');
    }

    /**
     * @param Builder $query
     * @param $value
     * @return Builder
     */
    public function scopeOrderId(Builder $query, $value): Builder
    {
        return $query->whereHas('orders', function (Builder $query) use ($value) {
            $query->where('orders.id', $value);
        });
    }

    /**
     * @param Builder $query
     * @param $value
     * @return Builder
     */
    public function scopePayer(Builder $query, $value): Builder
    {
        return $query->whereHas('shipmentItems', function (Builder $query) use ($value) {
            $query->whereHas('orderItem', function (Builder $query) use ($value) {
                $query->whereHas('order', function (Builder $query) use ($value) {
                    $query->whereHas('payer', function (Builder $query) use ($value) {
                        $query->where('companies.id', $value);
                    });
                });
            });
        });
    }

    /**
     * @param Builder $query
     * @param $value
     * @return Builder
     */
    public function scopeInvoiceOrNull(Builder $query, $value): Builder
    {
        return $query
            ->where('invoice_id', $value)
            ->orWhereNull('invoice_id');
    }

    /**
     * the related seller
     *
     * @return BelongsTo
     */
    public function seller(): BelongsTo
    {
        return $this->belongsTo(Company::class, 'seller_id');
    }

    /**
     * the related purchaser
     *
     * @return BelongsTo
     */
    public function purchaser(): BelongsTo
    {
        return $this->belongsTo(Company::class, 'purchaser_id');
    }

    /**
     * the title of this shipment
     *
     * @return string|null
     */
    public function getTitleAttribute(): ?string
    {
        return $this->getAttribute('shipment_number') ?? '#' . $this->id;
    }

    /**
     * the currency of this shipment
     *
     * @return string|null
     */
    public function getCurrencyAttribute(): ?string
    {
        return $this->orders()->first()->currency->code ?? Currency::where('default', true)->first()->code;
    }
}
