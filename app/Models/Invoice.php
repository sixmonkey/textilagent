<?php

namespace App\Models;

use App\Contracts\InvoiceNumberContract;
use App\Models\Traits\HasRelationships;
use App\Models\Traits\HasScopeDateBetween;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Stephenjude\DefaultModelSorting\Traits\DefaultOrderBy;

class Invoice extends Model
{
    use HasFactory;
    use DefaultOrderBy;
    use HasScopeDateBetween;
    use HasRelationships;

    /**
     * the default sort order column
     *
     * @var string
     */
    protected static string $orderByColumn = 'date';

    /**
     * the default sort order direction
     *
     * @var string
     */
    protected static string $orderByColumnDirection = 'DESC';

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $appends = [
        'title',
        'total_commission',
        'print_url',
    ];


    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'date',
        'due_date',
        'invoice_number',
        'receiver_id',
        'paid',
    ];

    /**
     * @var string[] $casts
     */
    protected $casts = [
        'paid' => 'boolean',
        'date' => 'datetime:Y-m-d',
    ];

    /**
     * @return void
     */
    protected static function booted(): void
    {
        static::creating(function (Invoice $invoice) {
            if (!$invoice->date) {
                $invoice->date = now();
            }

            if (!$invoice->invoice_number) {
                $invoiceNumbersService = app(InvoiceNumberContract::class);
                $invoice->invoice_number = $invoiceNumbersService->getInvoiceNumber($invoice);
            }
        });
        static::deleted(function (Invoice $invoice) {
            $invoice->shipments()->update(['invoice_id' => null]);
        });
    }

    /**
     * Get the receiver of the invoice.
     *
     * @return BelongsTo
     */
    public function receiver(): BelongsTo
    {
        return $this->belongsTo(Company::class, 'receiver_id');
    }

    /**
     * Get the shipments of the invoice.
     *
     * @return HasMany
     */
    public function shipments(): HasMany
    {
        return $this->hasMany(Shipment::class);
    }

    /**
     * Get the title of the invoice.
     *
     * @return string
     */
    public function getTitleAttribute() {
        return $this->invoice_number;
    }

    /**
     * Get the total amount of the invoice.
     *
     * @return float
     */
    public function getTotalCommissionAttribute() {
        return $this->shipments->sum('total_commission');
    }

    /**
     * the related download link
     *
     * @return string|null
     */
    public function getPrintUrlAttribute(): ?string
    {
        return (string)url(route('invoices.pdf', [$this->id]));
    }
}
