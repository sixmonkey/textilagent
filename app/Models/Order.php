<?php

namespace App\Models;

use App\Models\Traits\HasDownloads;
use App\Models\Traits\HasScopeDateBetween;
use App\Models\Traits\Searchable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Staudenmeir\EloquentHasManyDeep\HasManyDeep;
use Staudenmeir\EloquentHasManyDeep\HasRelationships;
use Stephenjude\DefaultModelSorting\Traits\DefaultOrderBy;

class Order extends Model implements HasMedia
{
    use HasFactory;
    use HasRelationships;
    use Searchable;
    use Traits\HasRelationships;
    use DefaultOrderBy;
    use HasScopeDateBetween;
    use InteractsWithMedia;
    use HasDownloads;

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'agent_id',
        'seller_id',
        'purchaser_id',
        'currency_id',
        'created_at',
        'updated_at',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array<string>
     */
    protected $fillable = [
        'contract',
        'customer_order_number',
        'date',
        'purchaser_pays',
        'completed'
    ];

    /**
     * the default sort order column
     *
     * @var string
     */
    protected static string $orderByColumn = 'contract';


    /**
     * the default sort order direction
     *
     * @var string
     */
    protected static string $orderByColumnDirection = 'desc';

    /**
     *
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'date' => 'datetime:Y-m-d',
        'purchaser_pays' => 'boolean',
        'completed' => 'boolean',
        'payed' => 'boolean',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var string[]
     */
    protected $appends = [
        'total',
        'title'
    ];

    protected static function booted()
    {
        self::updated(function ($order) {
            $order->shipments->each(function ($shipment) {
                $shipment->touch();
            });
        });
    }

    /**
     * the related currency
     *
     * @return BelongsTo
     */
    public function currency(): BelongsTo
    {
        return $this->belongsTo(Currency::class);
    }

    /**
     * the related agent
     *
     * @return BelongsTo
     */
    public function agent(): BelongsTo
    {
        return $this->belongsTo(User::class, 'agent_id');
    }

    /**
     * the related supplier
     *
     * @return BelongsTo
     */
    public function seller(): BelongsTo
    {
        return $this->belongsTo(Company::class, 'seller_id');
    }

    /**
     * the related supplier
     *
     * @return BelongsTo
     */
    public function purchaser(): BelongsTo
    {
        return $this->belongsTo(Company::class, 'purchaser_id');
    }

    /**
     * the related supplier
     *
     * @return BelongsTo
     */
    public function payer(): BelongsTo
    {
        return $this->belongsTo(Company::class, $this->getAttribute('purchaser_pays') ? 'purchaser_id' : 'seller_id');
    }

    /**
     * the related order items
     *
     * @return HasMany
     */
    public function orderItems(): HasMany
    {
        return $this->hasMany(OrderItem::class);
    }

    /**
     * the related order items
     *
     * @return HasMany
     */
    public function subAgents(): HasMany
    {
        return $this->hasMany(SubAgent::class);
    }

    /**
     * the related shipment items
     *
     * @return HasManyThrough
     */
    public function shipmentItems(): HasManyThrough
    {
        return $this->hasManyThrough(ShipmentItem::class, OrderItem::class);
    }

    /**
     * the related shipments
     *
     * @return HasManyDeep
     */
    public function shipments(): HasManyDeep
    {
        return $this->hasManyDeepFromRelations(
            $this->shipmentItems(),
            (new ShipmentItem())->shipment()
        )->groupBy('shipments.id');
    }

    /**
     * the total value of this order
     *
     * @return float
     */
    public function getTotalAttribute(): float
    {
        return (float)$this->orderItems()->selectRaw('sum(order_items.amount * order_items.price) as total_value')->first('total_value')->total_value;
    }

    /**
     * the title of this order
     *
     * @return string|null
     */
    public function getTitleAttribute(): ?string
    {
        return $this->getAttribute('contract') ?? '#' . $this->id;
    }

    /**
     * representation of this model in a search
     *
     * @return array
     */
    public function toSearchableArray(): array
    {
        return [
            'contract' => $this->contract,
        ];
    }
}
