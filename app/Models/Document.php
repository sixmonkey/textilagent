<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Stephenjude\DefaultModelSorting\Traits\DefaultOrderBy;

class Document extends Media
{
    use HasFactory;
    use DefaultOrderBy;

    /**
     * the default sort order column
     *
     * @var string
     */
    protected static string $orderByColumn = 'created_at';


    /**
     * the default sort order direction
     *
     * @var string
     */
    protected static string $orderByColumnDirection = 'desc';

    /**
     * @var string[] $appends
     */
    protected $appends = [
        'title',
        'download_url',
        'filesize',
    ];

    /**
     * get the download url for the document
     *
     * @return string
     */
    public function getDownloadUrlAttribute(): string
    {
        $related = $this->model()->getRelated();

        return (string)url(route($related->getTable() . '.documents.show', [$this->model()->first()->id, $this->id]) . '?download');
    }

    /**
     * get the filesize for the document
     *
     * @return string
     */
    public function getFilesizeAttribute(): string
    {
        return $this->human_readable_size;
    }

    /**
     * get the title for the document
     *
     * @return string
     */
    public function getTitleAttribute(): string
    {
        return $this->file_name;
    }
}
