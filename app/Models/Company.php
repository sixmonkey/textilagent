<?php

namespace App\Models;

use App\Models\Traits\HasCountry;
use App\Models\Traits\HasRelationships;
use App\Models\Traits\HasScopeHas;
use App\Models\Traits\Searchable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Stephenjude\DefaultModelSorting\Traits\DefaultOrderBy;

class Company extends Model
{
    use HasFactory;
    use HasCountry;
    use Searchable;
    use HasRelationships;
    use DefaultOrderBy;
    use HasScopeHas;

    /**
     * the default sort order column
     *
     * @var string
     */
    protected static string $orderByColumn = 'name';

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'phone',
        'address',
    ];

    /**
     * @param Builder $query
     * @param bool $value
     * @return mixed
     */
    public function scopeIsPayer(Builder $query, $value = true): mixed
    {
        $value = (bool)$value;
        $query
            ->whereHas('purchases', function (Builder $query) use ($value) {
                return $query->where('purchaser_pays', $value);
            })
            ->orWhereHas('sales', function (Builder $query) use ($value) {
                $query->where('purchaser_pays', !$value);
            });

        return $query;
    }

    /**
     * the purchases of this company.
     *
     * @return HasMany
     */
    public function purchases(): HasMany
    {
        return $this->hasMany(Order::class, 'purchaser_id');
    }

    /**
     * the supplies of this company.
     *
     * @return HasMany
     */
    public function sales(): HasMany
    {
        return $this->hasMany(Order::class, 'seller_id');
    }

    /**
     * the supplies of this company.
     *
     * @return HasMany
     */
    public function invoices(): HasMany
    {
        return $this->hasMany(Invoice::class, 'receiver_id');
    }

    /**
     * the outgoing shipment of this company.
     *
     * @return HasMany
     */
    public function outgoing_shipments(): HasMany
    {
        return $this->hasMany(Shipment::class, 'seller_id');
    }

    /**
     * the outgoing shipment of this company.
     *
     * @return HasMany
     */
    public function incoming_shipments(): HasMany
    {
        return $this->hasMany(Shipment::class, 'purchaser_id');
    }

    /**
     * representation of this model in a search
     *
     * @return array
     */
    public function toSearchableArray(): array
    {
        return [
            'name' => $this->name,
        ];
    }

    /**
     * get the users title
     *
     * @return string
     */
    public function getTitleAttribute(): string
    {
        return $this->name;
    }
}
