<?php

namespace Database\Factories;

use App\Models\Company;
use Illuminate\Database\Eloquent\Factories\Factory;

class InvoiceFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {

        $date = $this->faker->dateTimeBetween(now()->subYear(), now()->addYear());
        $due_date = $this->faker->dateTimeBetween($date, now()->addYear());
        return [
            'date' => $date,
            'due_date' => $due_date,
            'receiver_id' => Company::inRandomOrder()->first()->id ?? Company::factory()->create()->id,
            'paid' => $this->faker->boolean,
        ];
    }
}
