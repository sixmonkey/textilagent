<?php

namespace Database\Seeders;

use App\Models\Order;
use App\Models\Shipment;
use App\Models\ShipmentItem;
use Illuminate\Database\Seeder;

class ShipmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        !Shipment::all()->count() && Order::all()->each(function ($order) {
            $shipments = Shipment::factory()
                ->count(mt_rand(2, 5))
                ->make([
                    'seller_id' => $order->seller_id,
                    'purchaser_id' => $order->purchaser_id,
                ])
                ->each(fn($shipment) => $shipment->save([
                    'date' => $order->date->addDays(mt_rand(10, 90)),
                ]));

            $order->orderItems()->each(fn($orderItem) => ShipmentItem::factory()
                ->count(mt_rand(2, 5))
                ->create([
                    'order_item_id' => $orderItem->id,
                    'shipment_id' => $shipments->random()->id,
                ])
            );
        });
    }
}
