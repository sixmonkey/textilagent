<?php

namespace Database\Seeders;

use App\Models\Country;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        $admin_email = env('ADMIN_EMAIL','admin@textilagent.com');
        !User::where('email', $admin_email)->count() &&
        User::create([
                'admin' => true,
                'email' => $admin_email,
                'email_verified_at' => now(),
                'name' => env('ADMIN_NAME', 'Admin User'),
                'password' => Hash::make(env('ADMIN_PASSWORD', 'password')),
                'phone' => '',
                'address' => '',
                'country_id' => Country::where('iso_alpha_2', 'IT')->first()->id,
                'remember_token' => Str::random(10),
            ]);
    }
}
