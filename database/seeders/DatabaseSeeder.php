<?php

namespace Database\Seeders;

use App;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(): void
    {
        // always seed currencies, units, countries and default admin user
        $this->call(CurrencySeeder::class);
        $this->call(UnitSeeder::class);
        $this->call(CountrySeeder::class);
        $this->call(UserAdminSeeder::class);

        // seed test data only locally
        if (App::environment() === 'local') {
            $this->call(UserSeeder::class);
            $this->call(CompanySeeder::class);
            $this->call(OrderSeeder::class);
            $this->call(OrderItemSeeder::class);
            $this->call(SubAgentSeeder::class);
            $this->call(ShipmentSeeder::class);
            $this->call(InvoiceSeeder::class);
        }
    }
}
