<?php

namespace Database\Seeders;

use App\Models\SubAgent;
use Illuminate\Database\Seeder;

class SubAgentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        !SubAgent::all()->count() < 500 && SubAgent::factory()
            ->count(500)
            ->create();
    }
}
