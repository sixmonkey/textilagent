<?php

namespace Database\Seeders;

use App\Models\Currency;
use Illuminate\Database\Seeder;

class CurrencySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        Currency::updateOrCreate(['code' => 'EUR'], ['default' => true]);
        Currency::updateOrCreate(['code' => 'USD'], ['default' => false]);
    }
}
