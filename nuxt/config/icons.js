export default {
    email: 'mdi-email-outline',
    phone: 'mdi-phone',
    address: 'mdi-map-marker-outline',
    city: 'mdi-city',
    country: 'mdi-map',
    admin: 'mdi-account-cog',
}
