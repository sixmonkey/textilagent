import _ from "underscore";

export default {
    config: {
        fields: [],
    },
    data() {
        return {
            error: null,
            dialog: false,
            formData: {},
            formAction: '',
            config: this.$options.config ?? {}
        };
    },
    methods: {
        async storeObject() {
            if (this.$refs.observer.validate()) {
                this.$fetchState.pending = true
                try {
                    const url = this.formData.id
                        ? `${this.$route.path}/${this.formData.id}`
                        : this.$route.path
                    const method = this.formData.id ? 'put' : 'post'
                    const result = await this.$axios[method](url, this.formData)

                    this.closeForm()
                    this.$toast.success(
                        `Successfully saved ${result.data.data.title}`
                    )
                    this.onStore(result.data.data)
                } catch (error) {
                    console.dir(error.response.data.errors)
                    error.response?.data?.errors !== undefined &&
                    this.$refs.observer.setErrors(error.response.data.errors)
                    this.$toast.error(error?.response?.data?.message ?? error)
                }
                this.$fetchState.pending = false
            }
        },
        closeForm() {
            this.dialog = false
        },
        openAddForm() {
            this.formAction = `Add a new ${this.$singularize(this.$route.meta.title)}`

            this.formData = _.object(
                [..._.pluck(this.config.fields, 'name'), ...['id']],
                this.config.fields.map((field) => {
                    return field.default ?? null
                })
            )
            this.$refs.observer && this.$refs.observer.reset()
            this.dialog = true
        },
        openEditForm(data) {
            this.formAction = `Edit ${data.name}`
            this.formData = data
            this.dialog = true
        },
        fieldEditable(field, valid) {
            const editable = typeof field.editable === 'function'
                ? field.editable(this.formData, valid)
                : field.editable

            return editable !== false || this.formData.id === undefined
        },
        onStore(data) {

        }
    },
    watch: {
        dialog(newVal) {
            if (newVal) return
            if (this.$refs.form) this.$refs.form.reset()
            if (this.$refs.observer) this.$refs.observer.reset()
        },
    },
}
