export default {
    head() {
        return {
            title: `${this.title}${this.item?.title ? ` ${this.item.title}`  : ''}`,
        }
    },
    computed: {
        title() {
            return this.$route.meta.title ?? 'Unknown Title'
        },
        icon() {
            return this.$route.meta.icon ?? 'help-circle'
        },
    },
}
