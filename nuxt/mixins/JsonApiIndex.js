import _ from "lodash";
import cleanDeep from "clean-deep";

export default {
    data() {
        return {
            items: [],
            meta: {
                current_page: 1,
                per_page: this.$options.per_page ?? this.$config.page.default_size ?? 8,
                total: 0
            },
            itemsPerPageOptions: [10, ..._.range(25, this.$config.page.max_size, 25), parseInt(this.$config.page.max_size)],
            filter: this.$route.query?.filter ?? {},
            options: this.$queryToDatatable(),
        }
    },
    fetchOnServer: false,
    async fetch() {
        const result = await this.$jsonApi
            .get(this.$route.path, {
                include: this.$options.includes ?? '',
                ...this.$route.query,
                ...{
                    page: {
                        size: this.$route.query?.page?.size ?? this.$options.per_page ?? this.$config.page.default_size ?? 8,
                        number: this.$route.query?.page?.number ?? 1
                    },
                }
            })
            .then(result => {
                return result
            })
            .catch(error => {
                this.$toast.error(error.response?.data?.message ?? error.message)
                this.items = []
                this.meta = {}
            })

        this.items = result.data ? [...result.data] : []
        this.meta = result.meta
        this.options = this.$queryToDatatable();

        this.onFetch()
    },
    computed: {
        noDataText() {
            if (this.filterLength)
                return 'could not find any results matching this criteria!'

            return 'could not find any results!'
        },
        filterLength() {
            return Object.keys(cleanDeep(this.filter)).length
        }
    },
    watch: {
        '$route.query': '$fetch',
        filter: {
            handler() {
                this.options.page = 1
            },
            deep: true
        }
    },
    mounted() {
        this.$watch(vm => [vm.filter, vm.options], () => {
            this.$router.push({
                query: this.$datatableToJsonApi({
                        ...this.options,
                        ...{filter: this.filter},
                    }
                ),
            })
        }, {
            immediate: true,
            deep: true
        })
    },
    methods: {
        onFetch() {

        }
    }
}
