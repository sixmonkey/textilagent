import icons from '../config/icons'

export default ({ app }, inject) => {
  inject('getIcon', function (key) {
    return icons[key] ?? 'mdi-help'
  })
}
