import Vue from 'vue'
import {Doughnut, Line, mixins, Pie} from 'vue-chartjs'

Vue.component('line-chart', {
    extends: Line,
    props: ['data', 'options'],
    mixins: [mixins.reactiveProp],
    mounted() {
        this.renderChart(this.data, this.options)
    },
})

Vue.component('pie-chart', {
    extends: Pie,
    props: ['data', 'options'],
    mounted() {
        this.$nextTick(() => this.renderChart(this.data, this.options))
    },
})

Vue.component('doughnut-chart', {
    extends: Doughnut,
    props: ['data', 'options'],
    mounted() {
        this.$nextTick(() => this.renderChart(this.data, this.options))
    },
})
