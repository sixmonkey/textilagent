import { phone } from 'phone';

export default ({app}, inject) => {
    inject('phone', phone);
}
