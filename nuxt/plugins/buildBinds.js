import {get} from "simple-object-query";

const buildBinds = function (item, binds = {}, props = {}) {
    const result = {};
    if (typeof binds === 'object' && binds !== null) {
        Object.keys(binds).forEach(key => {
            if (typeof binds[key] === 'function') {
                result[key] = binds[key](item)
            } else
                result[key] = get(item, binds[key])
        })
    }
    return {...result, ...props};
}

export default ({app}, inject) => {
    inject('buildBinds', (item, binds, props) => buildBinds(item, binds, props))
}
